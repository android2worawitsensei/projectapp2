package com.thanawat.app_2

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.thanawat.app_2.databinding.FragmentHomeBinding
import com.thanawat.app_2.databinding.FragmentPlusBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER



class PlusFragment : Fragment() {


    private var _binding: FragmentPlusBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPlusBinding.inflate(inflater, container,false)
        // Inflate the layout for this fragment
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonEqual.setOnClickListener {
            var num1 = binding.inputStart.text.toString().toInt()
            var num2 = binding.inputEnd.text.toString().toInt()

            if(num2 != null || num1 != null){
                var result = num1+num2
                val action = PlusFragmentDirections.actionPlusFragmentToAnswerFragment(result = result.toString())
                Log.d("1234567",result.toString())
                view.findNavController().navigate(action)
            }

        }
    }
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    companion object {

    }
}