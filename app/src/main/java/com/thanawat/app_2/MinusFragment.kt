package com.thanawat.app_2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.thanawat.app_2.databinding.FragmentMinusBinding
import com.thanawat.app_2.databinding.FragmentPlusBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER



class MinusFragment : Fragment() {

    private var _binding: FragmentMinusBinding? = null
    private val binding get() = _binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMinusBinding.inflate(inflater, container,false)
        // Inflate the layout for this fragment
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonEqual.setOnClickListener {
            var num1 = binding.textStartMinus.text.toString().toInt()
            var num2 = binding.textEndMinus.text.toString().toInt()
            var result = num1-num2
            val action = MinusFragmentDirections.actionMinusFragmentToAnswerFragment(result = result.toString())
            view.findNavController().navigate(action)
        }
    }
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
    companion object {

    }
}